class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        lenList = len(nums)

        for key1 in range(lenList):
            result = target - nums[key1]

            if (result in nums):
                for key2 in range(lenList):
                    if (key1 != key2 and nums[key2] == result):
                        return[key1, key2]
                        break