# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def inorderTraversal(self, root: Optional[TreeNode]) -> List[int]:
        res = []

        def traversalTree(node):
            if (node is None):
                return

            traversalTree(node.left)
            res.append(node.val)
            traversalTree(node.right)

        traversalTree(root)
        
        return res