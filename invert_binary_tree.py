# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def invertTree(self, root: Optional[TreeNode]) -> Optional[TreeNode]:
        v = [root]
        while v:
            vn = []
            for x in v:
                if x:
                    if x.left and x.right:
                        temp = x.right
                        x.right = x.left
                        x.left = temp
                        vn += [x.left]
                        vn += [x.right]
                    elif not x.left and x.right:
                        x.left = x.right
                        x.right = None
                        vn += [x.left]
                    elif not x.right and x.left:
                        x.right = x.left
                        x.left = None
                        vn += [x.right]

            v = vn

        return root
