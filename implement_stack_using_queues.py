from queue import Queue

class MyStack:

    def __init__(self):
        self.q = Queue()
        self.qTemp = Queue()

    def push(self, x: int) -> None:
        self.q.put(x)

    def pop(self) -> int:
        target = 0

        for i in range(self.q.qsize()):
            if i == self.q.qsize()-1:
                target = self.q.get()
                break

            self.qTemp.put(self.q.get())
            self.q.put(self.qTemp.get())

        return target

    def top(self) -> int:
        target = 0

        for i in range(self.q.qsize()):
            target = self.q.get()
            self.qTemp.put(target)
            self.q.put(self.qTemp.get())

        return target

    def empty(self) -> bool:
        return self.q.empty()


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()