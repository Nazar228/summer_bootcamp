class Solution:
    def floodFill(self, image: List[List[int]], sr: int, sc: int, color: int) -> List[List[int]]:
        currentColor = image[sr][sc]
        height = len(image)
        width = len(image[0])

        def fill(sr, sc):
            if (0 <= sr < height and 0 <= sc < width):
                if (image[sr][sc] == currentColor and image[sr][sc] != color):
                    image[sr][sc] = color

                    fill(sr+1, sc)
                    fill(sr-1, sc)
                    fill(sr, sc+1)
                    fill(sr, sc-1)

        fill(sr, sc)

        return image