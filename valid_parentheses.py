from collections import deque

class Solution:
    def isValid(self, s: str) -> bool:
        stack = deque()

        for symbol in s:
            if (symbol in "([{"):
                stack.append(symbol)
            else:
                if (stack):
                    parentheses = stack.pop()
                    if (parentheses == "(" and symbol == ")"):
                        continue
                    elif (parentheses == "[" and symbol == "]"):
                        continue
                    elif (parentheses == "{" and symbol == "}"):
                        continue
                    else:
                        return False
                else:
                    return False

        if (not stack):
            return True