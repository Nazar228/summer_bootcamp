# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next

class Solution:
    def mergeTwoLists(self, list1: Optional[ListNode], list2: Optional[ListNode]) -> Optional[ListNode]:
        res = []

        current_node = list1
        while current_node is not None:
            res.append(current_node.val)
            current_node = current_node.next

        current_node = list2
        while current_node is not None:
            res.append(current_node.val)
            current_node = current_node.next

        res.sort()

        k = None
        for i in range(len(res)):
            res[i] = ListNode(res[i])
            k = res[0]
            if (i >= 1):
                res[i-1].next = res[i]

        return k

        