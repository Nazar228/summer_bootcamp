class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        lastWord = list(filter(None, s.split(" ")))[-1]
        
        return len(lastWord)