class Solution:
    def maxNumberOfBalloons(self, text: str) -> int:
        num = 0
        isStop = False
        word = list("balloon")
        text = list(text)

        while not isStop:
            for later in word:
                if (later in text):
                    text.remove(later)

                    if (later == word[-1]):
                        num += 1
                else:
                    isStop = True
                    break

        return num
