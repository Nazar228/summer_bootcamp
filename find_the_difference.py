class Solution:
    def findTheDifference(self, s: str, t: str) -> str:
        tArr = list(t)

        for sS in s:
            if sS in tArr:
                tArr.remove(sS)

        return tArr[0]